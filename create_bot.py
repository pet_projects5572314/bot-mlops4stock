import os

from aiogram import Bot, Dispatcher  # type: ignore
from aiogram.contrib.fsm_storage.memory import MemoryStorage  # type: ignore
from dotenv import load_dotenv

load_dotenv()
if "TOKEN_STAGE_BOT" in os.environ:
    TOKEN = str(os.getenv("TOKEN_STAGE_BOT"))
else:
    TOKEN = str(os.getenv("TOKEN"))

MLFLOW_TRACKING_URI = str(os.getenv("MLFLOW_TRACKING_URI"))

storage = MemoryStorage()
bot = Bot(TOKEN)
dp = Dispatcher(bot=bot, storage=storage)
