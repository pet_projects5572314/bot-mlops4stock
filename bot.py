import os

import telebot  # type: ignore
from dotenv import load_dotenv

from portfolio.request_mlops4stocks import main

load_dotenv()
MLFLOW_TRACKING_URI = str(os.getenv("MLFLOW_TRACKING_URI"))
TOKEN = str(os.getenv("TOKEN"))

bot = telebot.TeleBot(TOKEN)
print(__file__.split("/")[-1], TOKEN, MLFLOW_TRACKING_URI)


def portfolio_summary(tickers, weight_in_money) -> str:
    answer = ""
    answer += "Что бы получить базовый портфель вам нужно приобрести ценные бумаги: \n"

    sum_portfolio = 0
    for n, v in zip(tickers[:-1], weight_in_money[:-1]):
        sum_portfolio += v
        answer += f"<b>{n}</b> на сумму <em>{v:.2f}</em> руб.\n"
    answer += (
        f"Примерная стоимость портфель по текущим ценам {sum_portfolio:.2f} руб.\n"
    )
    answer += f"Остаток нераспределенных средств {round(weight_in_money[-1],2)} руб."
    return answer


@bot.message_handler(commands=["start"])
def start(message):
    # mess = f"Привет <b>{message.from_user.first_name}</b>"
    mess = f"Привет <b>{message.from_user.first_name}</b>"
    bot.send_message(message.chat.id, mess, parse_mode="html")


@bot.message_handler()
def get_user_text(message):
    # bot.send_message(message.chat.id, message, parse_mode="html")
    if message.text == "hello":
        bot.send_message(message.chat.id, "и тебе привет", parse_mode="html")
    elif message.text == "id":
        bot.send_message(
            message.chat.id, f"твой ID {message.from_user.id}", parse_mode="html"
        )
    elif message.text == "photo":
        photo = open("test_picture.png", "rb")
        bot.send_photo(message.chat.id, photo)
    elif message.text == "portfolio":
        tickers, weight_in_money = main(
            {"dt_to": "2023-06-16", "money": 1_000_000},
            f"{message.from_user.first_name}'s portfolio",
        )
        photo = open("portfolio.png", "rb")
        bot.send_photo(message.chat.id, photo)
        bot.send_message(
            message.chat.id,
            portfolio_summary(tickers, weight_in_money),
            parse_mode="html",
        )
    else:
        bot.send_message(message.chat.id, "нет такой комманды", parse_mode="html")


bot.polling(non_stop=True)
