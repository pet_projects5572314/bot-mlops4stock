FROM python:3.10
# copy project
COPY . /app
#COPY .env /app/.env
WORKDIR /app/

# install dependencies
RUN pip install --no-cache-dir -r requirements_prod.txt
# run app
CMD ["python", "fsm_bot.py"]