from datetime import date

from aiogram.types import KeyboardButton, ReplyKeyboardMarkup  # type: ignore


def get_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(
        KeyboardButton("/GMV"),  # type: ignore
        KeyboardButton("/GMV_L2"),  # type: ignore
        # KeyboardButton("/create_custom"),  # type: ignore
    )  # type: ignore
    kb.add(KeyboardButton("/help"))  # type: ignore
    return kb


def get_cancel_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton("/cancel"))  # type: ignore
    return kb


def get_date_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton(str(date.today())))  # type: ignore
    kb.add(KeyboardButton("/cancel"))  # type: ignore
    return kb


def get_amount_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton("1000000"))  # type: ignore
    kb.add(KeyboardButton("/cancel"))  # type: ignore
    return kb


def get_request_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton("/calculate"), KeyboardButton("/cancel"))  # type: ignore
    # kb.add()  # type: ignore
    return kb
