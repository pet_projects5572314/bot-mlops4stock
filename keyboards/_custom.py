from aiogram.types import KeyboardButton, ReplyKeyboardMarkup  # type: ignore


def get_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton("/create_basic"))  # type: ignore
    kb.add(KeyboardButton("/create_custom"))  # type: ignore
    return kb


def get_cancel_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton("/cancel"))  # type: ignore
    return kb


def get_yes_no_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton("yes"), KeyboardButton("no"))  # type: ignore
    kb.add(KeyboardButton("/cancel"))  # type: ignore
    return kb


def get_model_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardMarkup(resize_keyboard=True)  # type: ignore
    kb.add(KeyboardButton("MSR"), KeyboardButton("MVP"))  # type: ignore
    kb.add(KeyboardButton("/cancel"))  # type: ignore
    return kb
