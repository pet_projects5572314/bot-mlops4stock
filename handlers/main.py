from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher import FSMContext  # type: ignore

from keyboards.main import get_kb

HELP_COMMAND = """
<b>/start</b> - <em>Запустить бота</em>
<b>/help</b> - <em>Помощь</em>
<b>/GMV</b> - <em>Создать Global Minimum Variance Portfolio </em>
<b>/GMV_L2</b> - <em>Создать Global Minimum Variance Portfolio \
 c учетом комиссий и L2 регуляризацией</em>
"""


# @dp.message_handler(commands=["cancel"], state="*")  # ,
async def cmd_cancel(message: types.Message, state: FSMContext) -> None:
    if state is None:
        return

    await state.finish()
    await message.reply("Вы прервали создание портфеля", reply_markup=get_kb())


# @dp.message_handler(commands=["start"])
async def cmd_start(message: types.Message) -> None:
    await message.answer(
        text="""
        Привет давай созданим тебе портфель.
        Выбери: GMV or GMV_L2.
        для справки введи /help
        """,
        # parse_mode="HTML",
        # reply_markup=get_keyboard(),
        reply_markup=get_kb(),
    )


async def cmd_help(message: types.Message) -> None:
    await message.answer(
        text=HELP_COMMAND,
        parse_mode="HTML",
        # reply_markup=get_keyboard(),
        reply_markup=get_kb(),
    )


def register_main_handlers(dp: Dispatcher):
    dp.register_message_handler(cmd_cancel, commands=["cancel"], state="*")
    dp.register_message_handler(cmd_start, commands=["start"])
    dp.register_message_handler(cmd_help, commands=["help"])
