import os

from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher import FSMContext  # type: ignore

from create_bot import bot
# from handlers.basic import check_amount, load_date
from keyboards._custom import get_model_kb, get_yes_no_kb
from keyboards.main import (get_amount_kb, get_cancel_kb, get_date_kb, get_kb,
                            get_request_kb)
# from other import ProfileStatesGroup
from other import CustomFSM, portfolio_summary
from portfolio.request_mlops4stocks import main

PORTFOLIO_PICTURE_PATH = str(os.getenv("PORTFOLIO_PICTURE_PATH"))


# @dp.message_handler(commands=["create"])
async def cmd_create_custom(message: types.Message) -> None:
    await message.answer(
        text="Давайте создадим портфель custom",
        reply_markup=get_cancel_kb(),
    )
    await CustomFSM.date.set()
    await message.answer(
        "Введи дату, в формате YYYY-MM-DD, "
        "если хочешь использовать последнюю доступную дату "
        "нажми на кнопку внизу",
        reply_markup=get_date_kb(),
    )


# @dp.message_handler(state=ProfileStatesGroup.name)
async def load_date(message: types.Message, state: FSMContext) -> None:
    async with state.proxy() as data:
        data["date"] = message.text

    await CustomFSM.next()
    await message.answer(
        "Введи сумму на которую хотите разместить портфель в рублях без копеек",
        reply_markup=get_amount_kb(),
    )


# @dp.message_handler(
#     lambda message: not message.text.isdigit(), state=ProfileStatesGroup.age
# )
async def check_amount(message: types.Message) -> None:
    await message.reply(text="Это не число")


# @dp.message_handler(state=ProfileStatesGroup.age)
async def load_amount(message: types.Message, state: FSMContext) -> None:
    async with state.proxy() as data:
        data["amount"] = int(message.text)

    await CustomFSM.next()
    await message.answer(
        "Использовать ли фильтарию по тренду?\n"
        "Введите YES или NO.\n"
        "Для ручной фильтрации введите тикеры которые хотите "
        "использовать через пробел.\n"
        "пример: GAZP RASP LKOH",
        reply_markup=get_yes_no_kb(),
    )


async def load_filter(message: types.Message, state: FSMContext) -> None:
    async with state.proxy() as data:
        data["filter"] = message.text

    await CustomFSM.next()
    await message.answer(
        "Выберите модель расчета портфеля:\nMSR или MSV",
        reply_markup=get_model_kb(),
    )


async def load_portfolio_model(message: types.Message, state: FSMContext) -> None:
    async with state.proxy() as data:
        data["model"] = message.text

    await CustomFSM.next()
    async with state.proxy() as data:
        await bot.send_message(
            message.from_user.id,
            f"Вы ввели данные:\nдата: {data['date']}\nсумма: {data['amount']}\n"
            f"фильтр: {data['filter']}\nмодель: {data['model']}\n"
            "для расчета нажмите /calculate",
            parse_mode="html",
            reply_markup=get_request_kb(),
        )


# @dp.message_handler(state=ProfileStatesGroup.description)
async def load_request(message: types.Message, state: FSMContext) -> None:
    await bot.send_message(
        message.from_user.id,
        "Подгружаю данные с IMOEX и моделирую портфель BASIC\n"
        "приблизительное время ожидания 24 сек",
    )
    async with state.proxy() as data:
        REQUEST = {"dt_to": data["date"], "money": int(data["amount"])}

    tickers, weight_in_money = await main(
        REQUEST,
        "",
        f"{message.from_user.first_name}'s portfolio",
    )
    photo = open(PORTFOLIO_PICTURE_PATH, "rb")
    await bot.send_photo(chat_id=message.from_user.id, photo=photo)
    await bot.send_message(
        message.from_user.id,
        portfolio_summary(tickers, weight_in_money),
        parse_mode="html",
        reply_markup=get_kb(),
    )
    await state.finish()


def register_custom_handlers(dp: Dispatcher):
    dp.register_message_handler(
        cmd_create_custom, commands=["create_custom"], state=None
    )
    dp.register_message_handler(load_date, state=CustomFSM.date)
    dp.register_message_handler(
        check_amount,
        lambda message: not message.text.isdigit(),
        state=CustomFSM.amount,
    )
    dp.register_message_handler(load_amount, state=CustomFSM.amount)
    dp.register_message_handler(load_filter, state=CustomFSM.filter)
    dp.register_message_handler(load_portfolio_model, state=CustomFSM.portfolio)
    dp.register_message_handler(
        load_request, commands=["calculate"], state=CustomFSM.request
    )
