import os

from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher import FSMContext  # type: ignore

from create_bot import bot
from keyboards.main import (
    get_amount_kb,
    get_cancel_kb,
    get_date_kb,
    get_kb,
    get_request_kb,
)

# from other import ProfileStatesGroup
from other import BasicL2FSM, portfolio_summary
from portfolio.request_mlops4stocks import main

PORTFOLIO_PICTURE_PATH = str(os.getenv("PORTFOLIO_PICTURE_PATH"))


# @dp.message_handler(commands=["create"])
async def cmd_create_gmv_l2(message: types.Message) -> None:
    await message.answer(
        text="Создаем портфель Global Minimum Variance Portfolio \
 c учетом комиссий и L2 регуляризацией",
        reply_markup=get_cancel_kb(),
    )
    await BasicL2FSM.date.set()
    await message.answer(
        "Введи дату, в формате YYYY-MM-DD, "
        "если хочешь использовать последнюю доступную дату "
        "нажми на кнопку внизу",
        reply_markup=get_date_kb(),
    )


# @dp.message_handler(state=ProfileStatesGroup.name)
async def load_date(message: types.Message, state: FSMContext) -> None:
    async with state.proxy() as data:
        data["date"] = message.text

    await BasicL2FSM.next()
    await message.answer(
        "Введи сумму на которую хотите разместить портфель в рублях без копеек",
        reply_markup=get_amount_kb(),
    )


# @dp.message_handler(
#     lambda message: not message.text.isdigit(), state=ProfileStatesGroup.age
# )
async def check_amount(message: types.Message) -> None:
    await message.reply(text="Это не число")


# @dp.message_handler(state=ProfileStatesGroup.age)
async def load_amount(message: types.Message, state: FSMContext) -> None:
    async with state.proxy() as data:
        data["amount"] = int(message.text)

    await BasicL2FSM.next()
    async with state.proxy() as data:
        await bot.send_message(
            message.chat.id,
            f"Вы ввели данные:\nдата: {data['date']}\n"
            f"сумма: {data['amount']}\n"
            "для расчета нажмите /calculate",
            reply_markup=get_request_kb(),
        )


# @dp.message_handler(state=ProfileStatesGroup.description)
async def load_request(message: types.Message, state: FSMContext) -> None:
    await bot.send_message(
        message.chat.id,
        "Подгружаю данные с IMOEX и моделирую портфель GMV_L2\n"
        "приблизительное время ожидания 24 сек",
    )
    async with state.proxy() as data:
        REQUEST = {"dt_to": data["date"], "money": int(data["amount"])}

    tickers, weight_in_money = await main(
        REQUEST,
        "/optimize_gmv_l2",
        f"{message.chat.first_name}'s portfolio",
    )
    photo = open(PORTFOLIO_PICTURE_PATH, "rb")
    await bot.send_photo(chat_id=message.chat.id, photo=photo)
    await bot.send_message(
        message.chat.id,
        portfolio_summary(tickers, weight_in_money),
        parse_mode="html",
        reply_markup=get_kb(),
    )
    # data["description"] = message.text
    # await bot.send_photo(
    #     chat_id=message.chat.id,
    #     photo=data["photo"],
    #     caption=f'{data["name"]}, {data["age"]}\n{data["description"]}',
    # )
    # await message.answer(data)
    # await message.reply("Ваша анкета успешно созданна")
    await state.finish()


def register_basic_l2_handlers(dp: Dispatcher):
    dp.register_message_handler(cmd_create_gmv_l2, commands=["GMV_L2"], state=None)
    dp.register_message_handler(load_date, state=BasicL2FSM.date)
    dp.register_message_handler(
        check_amount,
        lambda message: not message.text.isdigit(),
        state=BasicL2FSM.amount,
    )
    dp.register_message_handler(load_amount, state=BasicL2FSM.amount)
    dp.register_message_handler(
        load_request, commands=["calculate"], state=BasicL2FSM.request
    )
