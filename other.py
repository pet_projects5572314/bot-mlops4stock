from aiogram.dispatcher.filters.state import State, StatesGroup  # type: ignore


def portfolio_summary(tickers, weight_in_money) -> str:
    answer = ""
    answer += "Что бы получить данный портфель вам нужно приобрести ценные бумаги: \n"

    sum_portfolio = 0
    for n, v in zip(tickers[:-1], weight_in_money[:-1]):
        sum_portfolio += v
        answer += f"<b>{n}</b> на сумму <em>{v:.2f}</em> руб.\n"
    answer += f"Примерная стоимость портфель по текущим ценам:\n<em>{sum_portfolio:.2f}\
        </em> руб.\n"
    answer += f"Остаток нераспределенных средств:\n<em>{round(weight_in_money[-1],2)}\
        </em> руб."
    return answer


# class ProfileStatesGroup(StatesGroup):
#     photo = State()
#     name = State()
#     age = State()
#     description = State()


class BasicFSM(StatesGroup):
    date = State()
    amount = State()
    request = State()


class BasicL2FSM(StatesGroup):
    date = State()
    amount = State()
    request = State()


class CustomFSM(StatesGroup):
    date = State()
    amount = State()
    filter = State()
    portfolio = State()
    request = State()
