run_bot:
	python bot.py

run_fsm_bot:
	python fsm_bot.py

## Lint using flake8
lint:
	python -m black .
	python -m isort .
	flake8 .
	mypy .
