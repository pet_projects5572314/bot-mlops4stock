import asyncio
import os

import aiohttp
import matplotlib.pyplot as plt  # type: ignore

# import requests  # type: ignore

# from dotenv import load_dotenv

# load_dotenv()
MODEL_SERVING_URL = str(os.getenv("MODEL_SERVING_URL"))
TEST_REQUEST = str(os.getenv("TEST_REQUEST"))
TEST_PORTFOLIO_NAME = str(os.getenv("TEST_PORTFOLIO_NAME"))
PORTFOLIO_PICTURE_PATH = str(os.getenv("PORTFOLIO_PICTURE_PATH"))


async def get_data(data: dict, model_url: str):
    async with aiohttp.ClientSession() as session:
        async with session.post(
            MODEL_SERVING_URL + model_url,
            json=data,
        ) as resp:
            pokemon = await resp.json()
            return pokemon


def format_raw_portfolio(resp_data):
    port = {el["name"]: el["val"] for el in resp_data["vals"]}
    port["free_money"] = resp_data["free_money"]
    return port


def create_plot(data, portfolio_name):
    fig, ax = plt.subplots()

    # Create a pie chart with the data
    wedges, texts, autotexts = ax.pie(
        data.values(), labels=data.keys(), autopct="%1.1f%%"
    )  # , autotexts #, autopct=lambda x: f'{abs(x):.0f}'

    # Set the text properties for the labels and values inside the pie chart sectors
    for text in texts:
        text.set_color("black")
        text.set_fontsize(8)
    for autotext in autotexts:
        autotext.set_color("black")
        autotext.set_fontsize(12)

    ax.set_title(portfolio_name, loc="left")
    plt.savefig(PORTFOLIO_PICTURE_PATH)


def create_plot2(data, portfolio_name):
    # Creating dataset
    tickers = []
    weight_in_money = []
    for k, v in data.items():
        tickers.append(k)
        weight_in_money.append(v)

    # Creating explode data
    explode = [round(0.075 * x, 1) if x > 5 else 0.1 for x in range(len(tickers))]

    # Creating color parameters
    colors = ("orange", "cyan", "brown", "grey", "indigo", "beige")

    # Wedge properties
    wp = {"linewidth": 1, "edgecolor": "green"}

    # Creating autocpt arguments
    def func(pct, allvalues):
        # absolute = int(pct / 100.*np.sum(allvalues))
        # return "{:.1f}%\n({:d} g)".format(pct, absolute)
        return "{:.1f}%".format(pct)

    # Creating plot
    fig, ax = plt.subplots(figsize=(10, 7))
    wedges, texts, autotexts = ax.pie(
        weight_in_money,
        autopct=lambda pct: func(pct, weight_in_money),
        explode=explode,
        labels=tickers,
        shadow=True,
        colors=colors,
        startangle=90,
        wedgeprops=wp,
        textprops=dict(color="magenta"),
    )

    # Adding legend
    ax.legend(
        wedges,
        tickers,
        title="Tickers",
        loc="center left",
        bbox_to_anchor=(1, 0, 0.5, 1),
    )

    plt.setp(autotexts, size=8, weight="bold")
    ax.set_title(portfolio_name, loc="left")

    # show plot
    plt.savefig(PORTFOLIO_PICTURE_PATH)
    return tickers, weight_in_money


async def main(request, model_url, portfolio_name):
    resp_data = await get_data(request, model_url)
    port = format_raw_portfolio(resp_data)
    tickers, weight_in_money = create_plot2(port, portfolio_name)
    return tickers, weight_in_money
    # print(response)
    # print()
    # print(response.json())


if __name__ == "__main__":
    model_url = "/optimize_gmv"
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(TEST_REQUEST, model_url, TEST_PORTFOLIO_NAME))
