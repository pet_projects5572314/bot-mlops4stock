# import sys
# sys.path.append(".")
from aiogram import executor  # type: ignore

from create_bot import MLFLOW_TRACKING_URI, TOKEN, dp
from handlers.gmv import register_basic_handlers
from handlers.gmv_l2 import register_basic_l2_handlers
# from handlers.custom import register_custom_handlers
from handlers.main import register_main_handlers


async def on_startup(_):
    print("Started: ", __file__.split("/")[-1], TOKEN, MLFLOW_TRACKING_URI)


register_main_handlers(dp)
register_basic_handlers(dp)
register_basic_l2_handlers(dp)
# register_custom_handlers(dp)

if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
